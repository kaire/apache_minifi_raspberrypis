import pandas as pd
from os import listdir
from os import mkdir
import datetime

# kataloog, kuhu Spark salvestas csv failid
path = 'kataloog/'

# Windowsi arvuti erinevus kellaserverist
offset = 0

filepaths = []
failinimed = [f for f in listdir(path) if f.endswith(".csv")]

for file in failinimed:
    filepaths.append (path + file)
    
print("failide arv " , len(filepaths))

# kasutatud: https://stackoverflow.com/questions/20906474/import-multiple-csv-files-into-pandas-and-concatenate-into-one-dataframe

andmete_list = []

for file in filepaths:
    try:
        andmed_yks_fail = pd.read_csv(file, delimiter=",")
        andmete_list.append(andmed_yks_fail)
    except pd.errors.EmptyDataError:
        print('tyhi fail, j2tan vahele')
        continue 


andmed_df = pd.concat(andmete_list, axis=0, ignore_index=True)

print("andmed_df shape", andmed_df.shape)

# andmed on suvalises järjekorras, tuleb sorteerida saatmise aja järgi
andmed_df_sorted = andmed_df.sort_values(by=['time'], ascending=True)

# ajatemplite andmetyyp on 'object'
# latentsuse arvutamiseks peab ajatempli formaat olema 'datetime'

andmed_df_sorted['time'] = pd.to_datetime(andmed_df_sorted['time'])
andmed_df_sorted['spark_time'] = pd.to_datetime(andmed_df_sorted['spark_time'])

# timezone eemaldamine sparki ajatemplist
andmed_df_sorted['spark_time'] = andmed_df_sorted['spark_time'].dt.tz_localize(None)

# uus veerg korrigeeritud ajaga, mis v6tab arvesse offset'i kellaserveriga
andmed_df_sorted['spark_time_offsetiga'] = andmed_df_sorted['spark_time'] + pd.to_timedelta(offset, unit = 's')

# latentsus - saatmise ja kohalej6udmise ajatemplite vahe
andmed_df_sorted['latency'] = andmed_df_sorted['spark_time_offsetiga'] - andmed_df_sorted['time']

# sisendi intervall iga rea vahel
andmed_df_sorted['input_interval_1'] = andmed_df_sorted['time'].diff()
andmed_df_sorted['row_interval'] = andmed_df_sorted['row'].diff()
andmed_df_sorted['input_interval'] = andmed_df_sorted['input_interval_1'] / andmed_df_sorted['row_interval']

# v2ljundi intervall iga rea vahel
andmed_df_sorted['output_interval_1'] = andmed_df_sorted['spark_time'].diff()
andmed_df_sorted['output_interval'] = andmed_df_sorted['output_interval_1'] / andmed_df_sorted['row_interval']

del andmed_df_sorted['output_interval_1']
del andmed_df_sorted['input_interval_1']
del andmed_df_sorted['row_interval']

# andmete saatmise kestus
time_max = andmed_df_sorted.time.max(axis = 0)
time_min = andmed_df_sorted.time.min(axis = 0)

andmete_saatmise_kestus = time_max - time_min
print("andmete_saatmise_kestus ", andmete_saatmise_kestus)

# saatmise intervalli arvutamiseks peab aeg olea sekundites, mitte ajatempli formaadis
andmete_saatmise_kestus_sek = andmete_saatmise_kestus.total_seconds()

# mitu rida võeti saadetavast algandmete failist
row_max = andmed_df_sorted.row.max(axis = 0)

# andmete sisselugemise intervall
andmete_sisselugemise_intervall = round((andmete_saatmise_kestus_sek / (row_max - 1)),2)
print("andmete_sisselugemise_intervall ", andmete_sisselugemise_intervall)

# Sparki vastuv6tmise kestus
spark_time_max = andmed_df_sorted.spark_time.max(axis = 0)
spark_time_min = andmed_df_sorted.spark_time.min(axis = 0)

Sparki_vatuv6tmise_kestus = spark_time_max - spark_time_min
print("Sparki_vatuv6tmise_kestus ", Sparki_vatuv6tmise_kestus)

andmete_vastuv6tmise_kestus_sek = Sparki_vatuv6tmise_kestus.total_seconds()

# andmete vastuv6tmise intervall
andmete_vastuv6tmise_intervall = round((andmete_vastuv6tmise_kestus_sek / (row_max - 1)),2)
print("andmete_vastuv6tmise_intervall ", andmete_vastuv6tmise_intervall)

cpu_all_iowait_max = andmed_df_sorted.cpu_all_iowait.max(axis = 0)
cpu_0_iowait_max = andmed_df_sorted.cpu_0_iowait.max(axis = 0)
cpu_1_iowait_max = andmed_df_sorted.cpu_1_iowait.max(axis = 0)
cpu_2_iowait_max = andmed_df_sorted.cpu_2_iowait.max(axis = 0)
cpu_3_iowait_max = andmed_df_sorted.cpu_3_iowait.max(axis = 0)

cpu_all_iowait_avg = andmed_df_sorted.cpu_all_iowait.mean(axis = 0)
cpu_0_iowait_avg = andmed_df_sorted.cpu_0_iowait.mean(axis = 0)
cpu_1_iowait_avg = andmed_df_sorted.cpu_1_iowait.mean(axis = 0)
cpu_2_iowait_avg = andmed_df_sorted.cpu_2_iowait.mean(axis = 0)
cpu_3_iowait_avg = andmed_df_sorted.cpu_3_iowait.mean(axis = 0)

cpu_all_idle_min = andmed_df_sorted.cpu_all_idle.min(axis = 0)
cpu_0_idle_min = andmed_df_sorted.cpu_0_idle.min(axis = 0)
cpu_1_idle_min = andmed_df_sorted.cpu_1_idle.min(axis = 0)
cpu_2_idle_min = andmed_df_sorted.cpu_2_idle.min(axis = 0)
cpu_3_idle_min = andmed_df_sorted.cpu_3_idle.min(axis = 0)

input_interval_min = andmed_df_sorted.input_interval.min(axis = 0)
input_interval_max = andmed_df_sorted.input_interval.max(axis = 0)


# andmete salvestamine failidesse
# andmete salvestamiseks alamkataloog 'statistika'

path_stat = path + 'statistika' 

try:
    mkdir(path_stat)
except FileExistsError:
    pass
    
# failide nimedesse ka kataloogi nimi
csv_kataloog = path.split("/")[-2]

# v2ljundisse on vaja ainult modifitseeritud aega
del andmed_df_sorted['spark_time']
# kõik Sparki salvestatud andmed ühte csv faili
andmefaili_nimi = csv_kataloog + "_data.csv"

andmed_df_sorted.to_csv(path_stat + "/" + andmefaili_nimi, sep=',', index=False)

# saatmise ja vastuvõtu kestuse info faili '_info.txt'
infofaili_nimi = csv_kataloog + "_info.txt"

andmete_saatmise_kestus_2 = str(andmete_saatmise_kestus).split('days')[1].strip()
Sparki_vatuv6tmise_kestus_2 = str(Sparki_vatuv6tmise_kestus).split('days')[1].strip()

f_info = open( path_stat + "/" + infofaili_nimi, 'w' )
f_info.write( 'andmete sisselugemise kestus: ' + andmete_saatmise_kestus_2 + '\n' )
f_info.write( 'andmete sisselugemise intervall sekundites keskmiselt : ' + str(andmete_sisselugemise_intervall) + '\n' )
f_info.write( 'Sparki vatuv6tmise kestus keskmiselt : ' + str(Sparki_vatuv6tmise_kestus_2) + '\n' )
f_info.write( 'Sparki vastuv6tmise intervall : ' + str(andmete_vastuv6tmise_intervall) + '\n' + '\n' )

f_info.write( 'input_interval_min : ' + str(round(input_interval_min.total_seconds(),2)) + '\n' )
f_info.write( 'input_interval_max : ' + str(round(input_interval_max.total_seconds(),2)) + '\n' + '\n' )

f_info.write( 'cpu_all_iowait_max : ' + str(cpu_all_iowait_max) + '\n' )
f_info.write( 'cpu_0_iowait_max : ' + str(cpu_0_iowait_max) + '\n' )
f_info.write( 'cpu_1_iowait_max : ' + str(cpu_1_iowait_max) + '\n' )
f_info.write( 'cpu_2_iowait_max : ' + str(cpu_2_iowait_max) + '\n' )
f_info.write( 'cpu_3_iowait_max : ' + str(cpu_3_iowait_max) + '\n' + '\n' )

f_info.write( 'cpu_all_iowait_avg : ' + str(round(cpu_all_iowait_avg,2)) + '\n' )
f_info.write( 'cpu_0_iowait_avg : ' + str(round(cpu_0_iowait_avg,2)) + '\n' )
f_info.write( 'cpu_1_iowait_avg : ' + str(round(cpu_1_iowait_avg,2)) + '\n' )
f_info.write( 'cpu_2_iowait_avg : ' + str(round(cpu_2_iowait_avg,2)) + '\n' )
f_info.write( 'cpu_3_iowait_avg : ' + str(round(cpu_3_iowait_avg,2)) + '\n' + '\n' )

f_info.write( 'cpu_all_idle_min : ' + str(cpu_all_idle_min) + '\n' )
f_info.write( 'cpu_0_idle_min : ' + str(cpu_0_idle_min) + '\n' )
f_info.write( 'cpu_1_idle_min : ' + str(cpu_1_idle_min) + '\n' )
f_info.write( 'cpu_2_idle_min : ' + str(cpu_2_idle_min) + '\n' )
f_info.write( 'cpu_3_idle_min : ' + str(cpu_3_idle_min) + '\n' )

f_info.close()

# statistika
# tuleb tabelist eemaldada veerud 'time' ja 'spark_time_offsetiga', sest nendele ei oska pandas leida keskmist
del andmed_df_sorted['time']
del andmed_df_sorted['spark_time_offsetiga']

# viis esimest rida tabelis
print(andmed_df_sorted.head())

def minMaxKeskm(andmed_1):
    return pd.Series(index=['min','max','average'],data=[andmed_1.min(),andmed_1.max(),andmed_1.mean()])

stat = andmed_df_sorted.apply(minMaxKeskm)
print(stat.head())

# statistika andmed ühte csv faili
stat_faili_nimi = csv_kataloog + "_stat.csv"

stat.to_csv(path_stat + "/" + stat_faili_nimi, sep=',', index=True)

