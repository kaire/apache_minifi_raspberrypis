# andmeridade genereerimine
# k2ivitamine k2surealt:
# python -u genereeri_andmed.py >> tee/sihtfailini/sihtfail.txt

# andmete kiireks faili salvestamiseks on vaja v2lja kommenteerida sleep k2sk

import random
import time
from datetime import datetime
import sys

# andmete loomise intervall
sleeptime = 1.5
# genereeritavate ridade arv
ridadeArv = 200

# muutujate algv22rtused
temperatuur = 6.01
co2 = 300
rida = 1

# temperatuuri muutused -20 kuni +20
aluminePiir = -20
yleminePiir = 20
co2aluminePiir = 250
co2yleminePiir = 400
# kui j6utakse yhe piirini, siis on vaja suunda muuta
suund = 1
suund2 = 1

for i in range(ridadeArv):
    temperatuuriMuutus = (float(random.randrange(1, 11)))/100 
    temperatuuriMuutus = temperatuuriMuutus * suund
    temperatuur = temperatuur + temperatuuriMuutus
    temperatuur = round(temperatuur, 2)
    if temperatuur >= yleminePiir:
        suund = -1    
    if temperatuur <= aluminePiir:
        suund = 1
        
    co2Muutus = (random.randrange(1, 11)) 
    co2Muutus = co2Muutus * suund2
    co2 = co2 + co2Muutus
    co2 = round(co2, 2)
    if co2 >= co2yleminePiir:
        suund2 = -1    
    if co2 <= co2aluminePiir:
        suund2 = 1
    
    # igale reale lisatakse ajatempel
    dateTimeObj = datetime.now()
    minifitime = dateTimeObj.strftime("%Y-%m-%d %H:%M:%S.%f")
    
    # iga andmerida on kujul: temp,co2;rida,ajatempel
    print(str(temperatuur) +"," + str(co2) + ";"+ str(rida) +  ","+ str(minifitime) + '\n')
    sys.stdout.flush()
    rida += 1
    
    time.sleep(sleeptime)



