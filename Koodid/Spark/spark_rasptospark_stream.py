import pyspark.sql.functions as sparkFun
from pyspark.sql.types import TimestampType, StringType, StructField, StructType, LongType, IntegerType, DoubleType
from pyspark.sql import SparkSession
import os

os.environ['PYSPARK_SUBMIT_ARGS'] = '--packages org.apache.spark:spark-sql-kafka-0-10_2.11:2.4.5 pyspark-shell'

spark = SparkSession \
    .builder \
    .appName("StructuredRaspToSpark") \
    .getOrCreate()

df = spark \
  .readStream \
  .format("kafka") \
  .option("kafka.bootstrap.servers", "172.17.66.109:9092") \
  .option("subscribe", "RaspToSpark") \
  .load().selectExpr("CAST(key AS STRING)", "CAST(value AS STRING)", "CAST(timestamp AS STRING)")

# andmed tulevad sellisel kujul
# {"RAM": "11.2", "temp": 6.41, "CPU_temp": "38.0", "time": "2020-04-26 16:08:48.555480", "cpu_3": "4.42", "cpu_2": "5.09", "cpu_all": "4.98", "cpu_0": "5.74", "cpu_1": "4.68", "row": 8}
schema = StructType([ StructField("time", StringType()),
                      StructField("temp", StringType()),
                      StructField("CO2", StringType()),
                      StructField("RAM", StringType()),
                      StructField("CPU_temp", StringType()),
                      StructField("cpu_all_usr", StringType()),
                      StructField("cpu_all_sys", StringType()),
                      StructField("cpu_all_iowait", StringType()),
                      StructField("cpu_all_idle", StringType()),
                      StructField("cpu_0_usr", StringType()),
                      StructField("cpu_0_sys", StringType()),
                      StructField("cpu_0_iowait", StringType()),
                      StructField("cpu_0_idle", StringType()),
                      StructField("cpu_1_usr", StringType()),
                      StructField("cpu_1_sys", StringType()),
                      StructField("cpu_1_iowait", StringType()),
                      StructField("cpu_1_idle", StringType()),
                      StructField("cpu_2_usr", StringType()),
                      StructField("cpu_2_sys", StringType()),
                      StructField("cpu_2_iowait", StringType()),
                      StructField("cpu_2_idle", StringType()),
                      StructField("cpu_3_usr", StringType()),
                      StructField("cpu_3_sys", StringType()),
                      StructField("cpu_3_iowait", StringType()),
                      StructField("cpu_3_idle", StringType()),
                      StructField("img_size_kB", StringType()),
                      StructField("row", StringType())
                      ])


data = df.select(sparkFun.from_json(df.value, schema).alias("data")).select("data.*", "*").withColumn("spark_time", sparkFun.current_timestamp())

# et poleks StructType
data2 = data.drop("data")


"""
# konsooli kuvamine
query = data2.writeStream \
    .outputMode("append") \
    .format("console") \
    .option("truncate", "false") \
    .start()
"""

query = data2 \
    .writeStream \
    .format("csv") \
    .option("format", "append")\
    .option("path", "kaust//kuhu//andmefailid//salvestada//alamkaust_mille_saprk_loob") \
    .option("header", "true") \
    .option("checkpointLocation", "kaust//kuhu//checkpoint//salvestada//alamkaust_mille_saprk_loob") \
    .outputMode("append") \
    .start()


query.awaitTermination()
