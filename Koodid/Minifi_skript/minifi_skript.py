#!/usr/bin/python

import subprocess
import time
from datetime import datetime
import base64
import os
import sys

attrMap = {}

img_file = "/tee/pildifailini/pilt.jpg"

# ExecuteStreamCommand protsessoris ette antud atribuutide sisselugemine
andmed = sys.argv[1]
andmed2 = andmed.split(",")
andmed3 = sys.argv[2]
attrMap["temp"] = andmed2[0]
attrMap["CO2"] = andmed2[1]
attrMap["row"] = andmed3


#CPU tuumade info
cpu_com = 'mpstat -P ALL 1 1' 
cpu_process = subprocess.Popen([cpu_com], stdout=subprocess.PIPE, shell=True)
(cpu_output, cpu_error) = cpu_process.communicate()
cpu_output_split = cpu_output.splitlines()

for row,line in enumerate(cpu_output_split):
    if row in range(3,8):
        elems = line.split()
        usr = round(float(elems[3]),2)
        sys = round(float(elems[5]),2)
        iowait = round(float(elems[6]),2)
        idle = round(float(elems[12]),2)
        if row ==3:
            attrMap["cpu_all_usr"] = str(usr)
            attrMap["cpu_all_sys"] = str(sys)
            attrMap["cpu_all_iowait"] = str(iowait)
            attrMap["cpu_all_idle"] = str(idle)
        else:
            attrMap["cpu_"+str(row-4)+"_usr"] = str(usr)
            attrMap["cpu_"+str(row-4)+"_sys"] = str(sys)
            attrMap["cpu_"+str(row-4)+"_iowait"] = str(iowait)
            attrMap["cpu_"+str(row-4)+"_idle"] = str(idle)

# CPU temperatuur
cpu_temp_com = 'vcgencmd measure_temp'  
cpu_temp_process = subprocess.Popen([cpu_temp_com], stdout=subprocess.PIPE, shell=True)
(cpu_temp_output, cpu_temp_error) = cpu_temp_process.communicate()
cpu_temp_val = cpu_temp_output.replace("temp=","").replace("'C\n","")

#RAM
ram_com = "free | awk '/Mem:/ {print $2,$3}'"
ram_process = subprocess.Popen([ram_com], stdout=subprocess.PIPE, shell=True)
(ram_output, ram_error) = ram_process.communicate()
ram_output_str = str(ram_output)
ram_output_strip = ram_output_str.strip()
ram_output_split = ram_output_strip.split()
ram_used = float(ram_output_split[1])
ram_total = float(ram_output_split[0])
suhe = ram_used/ram_total
RAM_usage_1 = suhe*100
RAM_usage_2 = round(RAM_usage_1, 1)
RAM_usage = str(RAM_usage_2)

attrMap["RAM"] = RAM_usage
attrMap["CPU_temp"] = cpu_temp_val 

# pildi avamine
with open(img_file, "rb") as img:  
    img_str = base64.b64encode(img.read())
attrMap["img"] = img_str
img_size_kB = round((os.path.getsize(img_file)/1024),1)
attrMap["img_size_kB"] = img_size_kB

# ajatempel
dateTimeObj = datetime.now()
minifitime = dateTimeObj.strftime("%Y-%m-%d %H:%M:%S.%f")
attrMap["time"] = minifitime

# saadab andmed edasi voogu
print(attrMap)
    