# V6tab andmefailist rea kaupa temperatuuri n2ite, v6rdleb ja saadetavatele temperatuuridele
# lisab juurde ka CPU ja RAM info

# kasutatud on koodi lehelt: https://www.raspberrypi.org/forums/viewtopic.php?t=22180

from json import dumps
from datetime import datetime
from kafka import KafkaProducer
from time import sleep
import time
import os
import subprocess
import base64

failiNimi = "andmefail.txt"
img_file = "pilt.jpg"

path = 'tee/andmefailini/'

file=open(path+failiNimi, "r")

reaNr = 0

# Produceri loomine
producer = KafkaProducer(bootstrap_servers=['172.17.66.109:9092'], value_serializer=lambda x: dumps(x).encode('utf-8'))

while True:
    # aeg, kui rea sisse lugemist alustati
    tstart = time.time()
    
    rida = file.readline()
    if not rida:
        break
    rida = rida.strip()
    data = rida.split(",")
    temp_1 = float(data[0])
    co2 = float(data[1])
    reaNr += 1
    
    saatmiseks_dict = dict()
    
    #CPU tuumade andmed
    cpu_com = 'mpstat -P ALL 1 1' # annab keskmise cpu kasutuse arvuti k2ivitamisest alates
    cpu_process = subprocess.Popen([cpu_com], stdout=subprocess.PIPE, shell=True)
    (cpu_output, cpu_error) = cpu_process.communicate()
    cpu_output_split = cpu_output.splitlines()
    
    for row,line in enumerate(cpu_output_split):
        if row in range(3,8):
            elems = line.split()
            usr = round(float(elems[3]),2)
            sys = round(float(elems[5]),2)
            iowait = round(float(elems[6]),2)
            idle = round(float(elems[12]),2)
            if row ==3:
                saatmiseks_dict["cpu_all_usr"] = str(usr)
                saatmiseks_dict["cpu_all_sys"] = str(sys)
                saatmiseks_dict["cpu_all_iowait"] = str(iowait)
                saatmiseks_dict["cpu_all_idle"] = str(idle)
            else:
                saatmiseks_dict["cpu_"+str(row-4)+"_usr"] = str(usr)
                saatmiseks_dict["cpu_"+str(row-4)+"_sys"] = str(sys)
                saatmiseks_dict["cpu_"+str(row-4)+"_iowait"] = str(iowait)
                saatmiseks_dict["cpu_"+str(row-4)+"_idle"] = str(idle)
    
    cpu_temp_com = 'vcgencmd measure_temp'
    cpu_temp_process = subprocess.Popen([cpu_temp_com], stdout=subprocess.PIPE, shell=True)
    (cpu_temp_output, cpu_temp_error) = cpu_temp_process.communicate()
    cpu_temp_val = cpu_temp_output.replace("temp=","").replace("'C\n","")
    saatmiseks_dict["CPU_temp"] = cpu_temp_val
    
    #RAM
    ram_com = "free | awk '/Mem:/ {print $2,$3}'"
    ram_process = subprocess.Popen([ram_com], stdout=subprocess.PIPE, shell=True)
    (ram_output, ram_error) = ram_process.communicate()
    ram_output_str = str(ram_output)
    ram_output_strip = ram_output_str.strip()
    ram_output_split = ram_output_strip.split()
    ram_used = float(ram_output_split[1])
    ram_total = float(ram_output_split[0])
    suhe = ram_used/ram_total
    RAM_usage_1 = suhe*100
    RAM_usage_2 = round(RAM_usage_1, 1)
    RAM_usage = str(RAM_usage_2)
    saatmiseks_dict["RAM"] = RAM_usage  
    
    saatmiseks_dict["row"] = reaNr
    saatmiseks_dict["temp"] = temp_1
    saatmiseks_dict["CO2"] = co2
    
    # pildifaili viimine base64 kujule
    with open(img_file, "rb") as img: 
        img_str = base64.b64encode(img.read())

    img_size_kB = round((os.path.getsize(img_file)/1024),1)
    
    saatmiseks_dict["img"] = img_str
    saatmiseks_dict["img_size_kB"] = img_size_kB
    
    # ajatempel
    dateTimeObj = datetime.now()
    timestampStr = dateTimeObj.strftime("%Y-%m-%d %H:%M:%S.%f")
    saatmiseks_dict["time"] = timestampStr
    
    # andmete saatmine Kafka topicusse
    producer.send('RaspToSpark', value=saatmiseks_dict)
    # ootame, et andmed oleks saadetud, enne edasi ei l2he
    producer.flush()
    
    # aeg, kui k6ik andmete t88tlemise protsessid on valmis
    tend = time.time()
    # lahutame intervallist juba kulunud aja 
    sleeptime = max(0, 3 - (tend-tstart))
    sleep(sleeptime)
    

file.close()

