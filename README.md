# README #


Selles repositooriumis on Tartu Ülikooli tudengi lõputöö raames kasutatud koodid.

Lõputöö raames uuriti Apache MiNiFi raamistiku kasutamise mõistlikkust servaseadmes läbi praktilise katsetuse, 
valides riistvaraliseks lahenduseks Raspberry Pi. Katsetuse käigus võrreldi Raspberry Pi ressursside kulu ning 
töödeldavate andmete mahtu ilma raamistikuta ning MiNiFi’d kasutades. Vaadeldi Raspberry Pi protsessori tuumade 
hõivatust ja temperatuuri. Samuti koguti andmeid mälu kasutuse, töödeldavate andmete mahu ning andmevootöötluse 
protsessi pikkuse kohta.

## Riistvara ja konfiguratsioon

Katsetes kasutati MiNiFi Java versiooni. Servaseadmena kasutati 4 tuumaga Raspberry Pi 4. Salvestuseadmena kasutati
U3 kiirusklassi 16GB SD-kaarti.
Lisaks oli kasutuses Tartu Ülikooli OpenStack  pilveteenuse keskkonnas loodud virtuaalmasin. Virtuaalmasinale 
eraldati 8 tuuma, 32GB RAM ja 20GB kettaruumi. Operatsioonisüsteemiks oli Ubuntu 18.04. 
Virtuaalmasinas seati üles Kafka ning Dockeri konteineris NiFi 1.11.4.
Kafka konfiguratsioonifailides tuli teha muudatusi, et Kafka kuulaks ka väljastpoolt virtuaalmasinat tulevaid pakette. 
Raspberry Pi's tuli lisada /etc/hosts faili rida NiFi image'i nimelahendamiseks.
Andmete Kafkast kuulamiseks kasutati eraldiseisvat sülearvuti, milles seati PyCharmi abil üles Apache Sparki raamistik.

## Koodid

Repositooriumisse lisatud koodide hulgas on Pythoni kood, mida kasutati testides andmete genereerimiseks. Samuti
on olemas MiNiFi poolt käivitatav skript Raspberry Pi süsteemi parameetrite kohta andmete kogumiseks.
Lisatud on ka Pythoni kood Sparkis käivitamiseks ning eraldiseisev kood andmete analüüsimiseks.

## NiFi templates

Templates alamkausta on lisatud MiNiFi flow XML-fail. MiNiFi's käiviamiseks on lisatud peale konverteerimist 
vajalike muudatustega config.yml fail. Lisaks on alamkaustas NiFi's töötava pordi ja Kafkasse andmeid saatva 
protsessori template.

